# Daylevator
Daylevator is a lightweight elevator plugin for Spigot Servers with a version of 1.12.2 or higher.

It grants every daylight detector a new function based on the player behavior.  
If a player sneaks on top of a daylight detector, he'll get teleported on top of the closest daylight detector below.  
In return, if the player decided to jump on top of a daylight detector, he'll get teleported to the closest daylight detector above.

This plugin is [based on this request](https://minecraft-server.eu/forum/threads/58358/).

## Installation
Download the [latest build from GitLab](https://gitlab.com/Ptilopsis/daylevator/-/jobs/artifacts/master/raw/build/libs/Daylevator-0.1-SNAPSHOT.jar?job=build).  
Then move the .jar into the `plugins/` directory.  

## Authors
- **[Ptilopsis](https://gitlab.com/Ptilopsis)** - Initial implementation of this project.

See also the list of [contributers](https://gitlab.com/Ptilopsis/daylevator/-/graphs/master) who participated in this project.

## License
This project is licensed under the Apache 2.0 License.  
Please check out the [LICENSE.md](LICENSE.md) file for more information.