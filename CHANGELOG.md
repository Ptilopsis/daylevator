# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2020-01-25
### Added
- Jump on top of a daylight detector to teleport on top of the nearest daylight detector above.
- Sneak on top of a daylight detector to teleport on top of the nearest daylight detector underneath.
- Validation to ensure the player won't get teleported into a wall.

[0.1.0]: https://gitlab.com/Ptilopsis/daylevator/-/tags/0.1.0