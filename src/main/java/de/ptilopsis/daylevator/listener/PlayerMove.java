/*
   Copyright 2020 Ptilopsis

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package de.ptilopsis.daylevator.listener;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public final class PlayerMove implements Listener {
    @EventHandler
    private void onJump(PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final Location location = player.getLocation();
        final Block block = location.getBlock();

        if(event.getFrom().getY() < event.getTo().getY() && block.getType() == Material.DAYLIGHT_DETECTOR) {
            for (int i = 1; i + location.getY() <= 255; ++i) {
                if (block.getRelative(BlockFace.UP, i).getType() == Material.DAYLIGHT_DETECTOR
                        && block.getRelative(BlockFace.UP, i + 1).getType() == Material.AIR
                        && block.getRelative(BlockFace.UP, i + 2).getType() == Material.AIR) {
                    final Location target = player.getLocation();
                    target.setY(location.getBlockY() + i + .375);

                    player.teleport(target);
                    break;
                }
            }
        }
    }
}
